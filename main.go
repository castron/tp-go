package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"time"
)

func hourHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		var t = time.Now()
		fmt.Fprintf(w, fmt.Sprintf("%02dh%02d", t.Hour(), t.Minute()))
	}
}

func saveHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		var author string
		var text string

		for key, value := range req.PostForm {
			switch key {
				case "author":
					author = value[0]
				case "entry":
					text = value[0]
			}
		}
		fmt.Fprintln(w, fmt.Sprintf("%s: %s", author, text))

		saveFile, err := os.OpenFile("./save.data", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
		defer saveFile.Close()

		w := bufio.NewWriter(saveFile)
		if err == nil {
			fmt.Fprintf(w, fmt.Sprintf("%s\n", text))
		}
		w.Flush()
	}
}

func entriesHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		saveData, err := os.ReadFile("./save.data")
		if err == nil {
			fmt.Fprintln(w, string(saveData))
		}
	}
}

func main() {
	http.HandleFunc("/", hourHandler)
	http.HandleFunc("/add", saveHandler)
	http.HandleFunc("/entries", entriesHandler)
	http.ListenAndServe(":9000", nil)
}